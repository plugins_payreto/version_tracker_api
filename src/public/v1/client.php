<?php
namespace v1;
use Luracast\Restler\RestException;
require_once './../public/db/pdo/clients.php';

class Client
{
    public $db;

    function __construct()
    {
        $this->client   = new \DB_PDO_Clients();
    }

    function get()
    {
        return $this->client->get_name();
    }
}

