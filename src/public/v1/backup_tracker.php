<?php
namespace v1;
use Luracast\Restler\RestException;
require_once './../public/db/pdo/merchants.php';
require_once './../public/db/pdo/clients.php';
require_once './../public/db/pdo/shop_systems.php';

class Tracker
{
    public $db;

    static $FIELDS = array('shop_version', 'plugin_version', 'client', 'hash', 'ip_address', 'transaction_mode');

    function __construct()
    {
        $this->merchant = new \DB_PDO_Merchants();
        $this->client   = new \DB_PDO_Clients();
        $this->shop     = new \DB_PDO_Shop_systems();
    }

    function post($request_data = NULL)
    {
        if(isset($request_data['ip_address'])){
            $last_data      = $this->merchant->validate_ip($request_data['ip_address']);
            if($last_data){
                $last_data_date = date_parse_from_format('Y-m-d H:i:s', $last_data['timestamp']);
                $current_data_date = date_parse_from_format('Y-m-d H:i:s', date('Y-m-d H:i:s', time()));
                $is_data_valid = $this->_validate_date($last_data_date, $current_data_date);
                if(!$is_data_valid){
                    throw new RestException(202, "Request accepted but the maximum request per day is exceeded");    
                }
            }

        }
        if(isset($request_data['shop_system'])){
            $exist['shop_system'] = $this->shop->get_by_name($request_data['shop_system']); 
        }
        if(isset($request_data['client'])){
            $exist['client']      = $this->client->get_by_name($request_data['client']);
        }
        $this->_validate_total_per_merchant($request_data['merchant_id']);
        $request_data = $this->_validate_value($request_data, $exist);
        $merchant_data = $this->_validate_data($request_data, $exist);
        return $this->merchant->insert($merchant_data, $request_data);
    }

    private function _validate_date($last_data_date, $current_data_date){
         if($current_data_date['year'] > $last_data_date['year']){
            return true;
         }
         else if ($current_data_date['year'] == $last_data_date['year']){
            if($current_data_date['month'] > $last_data_date['month']){
                return true;
                
            }
            else if ($current_data_date['month'] == $last_data_date['month']){
                 if($current_data_date['day'] > $last_data_date['day']){
                    return true;
                 }
            }
         }
         return false;

    }

    private function _validate_total_per_merchant($merchant_id){
        $total_merchant = $this->merchant->get_count_by_merchant_id($merchant_id);
        if($total_merchant > 30){
            $this->merchant->delete_first_data($merchant_id);
        }
        return true;

    }

    private function _validate_value($data, $exist)
    {
        $encrypt_hash = md5($data['shop_version'].$data['plugin_version'].$data['client']);
        if($data['hash'] !== $encrypt_hash){
            throw new RestException(400, "Invalid hash");    
        }
        foreach (tracker::$FIELDS as $field) {
            if (!isset($data[$field])){
                throw new RestException(400, "Missing $field value");
            }
        }
        if(!isset($data['email']) OR empty($data['email'])){
            $data['email'] = NULL;
        }
        if(!isset($data['merchant_id']) OR empty($data['merchant_id'])){
            $data['merchant_id'] = NULL;
        }
        if(!isset($data['shop_system']) OR !$exist['shop_system'] OR  empty($data['shop_system'])){
            $data['shop_system'] = NULL;
        }
        if(!isset($data['shop_url']) OR empty($data['shop_url'])){
            $data['shop_url'] = NULL;
        }
        if( !isset($data['shop_system']) OR !$exist['client']){
            $data['client'] = NULL;
        }   
        unset($data['hash']);
        return $data;
    }

    private function _validate_data($data, $exist){
        if($exist['client']){
            $data['client'] = $exist['client']['id'];
        }
        if(!empty($data['shop_system'])){
            $data['shop_system'] = $exist['shop_system']['id'];
        }
        return $data;
    }
}

