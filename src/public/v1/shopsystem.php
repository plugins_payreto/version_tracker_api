<?php
namespace v1;
use Luracast\Restler\RestException;
require_once './../public/db/pdo/shop_systems.php';

class Shopsystem
{
    public $db;

    function __construct()
    {
        $this->shop_system   = new \DB_PDO_Shop_systems();
    }

    function get()
    {
        return $this->shop_system->get_name();
    }
}

