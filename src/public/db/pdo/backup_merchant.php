<?php

/**
 * MySQL DB. All data is stored in data_pdo_mysql database
 * Create an empty MySQL database and set the dbname, username
 * and password below
 *
 * This class will create the table with sample data
 * automatically on first `get` or `get($id)` request
 */
use Luracast\Restler\RestException;

class DB_PDO_Merchants
{
    private $db;
    private $_db_hostname;
    private $_db_database;
    private $_db_username;
    private $_db_password;
    protected $_table_name          = 'merchants';
    protected $_primary_key         = 'id';
    protected $_merchant_id         = 'merchant_id';
    protected $_ip_address          = 'ip_address';
    protected $_email               = 'email';
    protected $_shop_url            = 'shop_url';
    protected $_shop_version        = 'shop_version';
    protected $_shop_system         = 'shop_system';
    protected $_plugin_version      = 'plugin_version';
    protected $_client              = 'client';
    protected $_timestamp           = 'timestamp';


    function __construct()
    {
        $this->_db_hostname      = getenv('DB_HOSTNAME');
        $this->_db_database      = getenv('DB_DATABASE');
        $this->_db_username      = getenv('DB_USERNAME');
        $this->_db_password      = getenv('DB_PASSWORD');
        try {
            //Make sure you are using UTF-8
            $options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8');

            //Update the dbname username and password to suit your server
            $this->db = new PDO(
                'mysql:host='.$this->_db_hostname.';dbname='.$this->_db_database.'',
                $this->_db_username,
                $this->_db_password,
                $options
            );
            $this->db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,
                PDO::FETCH_ASSOC);

            //If you are using older version of PHP and having issues with Unicode
            //uncomment the following line
            //$this->db->exec("SET NAMES utf8");

        } catch (PDOException $e) {
            throw new RestException(501, 'MySQL: ' . $e->getMessage());
        }
    }

    function delete_first_data($merchant_id)
    {
        if (!$this->db->prepare('DELETE FROM '.$this->_table_name.' WHERE '.$this->_merchant_id.' = ? ORDER BY '.$this->_primary_key.' ASC LIMIT 1')->execute(array($merchant_id)))
            return FALSE;
        return true;
    }

    function get_count_by_merchant_id($merchant_id, $installTableOnFailure = FALSE, $request_data=FALSE)
    {
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sql = $this->db->prepare('SELECT COUNT(*) AS total_merchant FROM '.$this->_table_name.' WHERE '.$this->_merchant_id.' = :merchant_id');
            $sql->execute(array(':merchant_id' => $merchant_id));
            $result = $sql->fetch();
            return $result['total_merchant'];
        } catch (PDOException $e) {
            throw new RestException(501, 'MySQL: ' . $e->getMessage());
        }
    }

    function get($id, $installTableOnFailure = FALSE, $request_data=FALSE)
    {
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sql = $this->db->prepare('SELECT * FROM '.$this->_table_name.' WHERE '.$this->_primary_key.' = :id');
            $sql->execute(array(':id' => $id));
            $result = $this->id2int($sql->fetch());
            $result['client']      = $request_data['client'];
            $result['shop_system'] = $request_data['shop_system'];
            return $result;
        } catch (PDOException $e) {
            throw new RestException(501, 'MySQL: ' . $e->getMessage());
        }
    }
    function validate_ip($ip_address = false){
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sql = $this->db->prepare('SELECT * FROM '.$this->_table_name.' WHERE '.$this->_ip_address.' = :ip_address order by '.$this->_timestamp.' desc limit 1');
            $sql->execute(array(':ip_address' => $ip_address));
            return $this->id2int($sql->fetch());
        } catch (PDOException $e) {
            throw new RestException(501, 'MySQL: ' . $e->getMessage());
        }
    }
    function insert($data, $request_data)
    {
        $sql = $this->db->prepare("INSERT INTO ".$this->_table_name." (".$this->_merchant_id.",".$this->_ip_address.",".$this->_email.", ".$this->_shop_url.", ".$this->_shop_version.", ".$this->_shop_system.",".$this->_plugin_version.",".$this->_client.",".$this->_timestamp.") 
            VALUES (:merchant_id,:ip_address,:email, :shop_url, :shop_version, :shop_system, :plugin_version, :client, :timestamp)");
        $execute_sql = $sql->execute(array(
            ':merchant_id'  => $data['merchant_id'], 
            ':ip_address'  => $data['ip_address'], 
            ':email'  => $data['email'], 
            ':shop_url'  => $data['shop_url'], 
            ':shop_version' => $data['shop_version'], 
            ':shop_system' => $data['shop_system'], 
            ':plugin_version' => $data['plugin_version'], 
            ':client' => $data['client'], 
            ':timestamp' => date('Y-m-d H:i:s', time())
        ));
        if (!$execute_sql){
            return FALSE;
        }
        return $this->get($this->db->lastInsertId(), FALSE, $request_data);
    }

    private function id2int($r)
    {
        if (is_array($r)) {
            if (isset($r['id'])) {
                $r['id'] = intval($r['id']);
            } else {
                foreach ($r as &$r0) {
                    $r0['id'] = intval($r0['id']);
                }
            }
        }
        return $r;
    }

    private function result($data){

    }
}

