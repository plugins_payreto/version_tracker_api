<?php

/**
 * MySQL DB. All data is stored in data_pdo_mysql database
 * Create an empty MySQL database and set the dbname, username
 * and password below
 *
 * This class will create the table with sample data
 * automatically on first `get` or `get($id)` request
 */
use Luracast\Restler\RestException;

class DB_PDO_Clients
{
    private $db;
    private $_db_hostname;
    private $_db_database;
    private $_db_username;
    private $_db_password;
    protected $_table_name  = 'clients';
    protected $_primary_key = 'id';
    protected $_name        = 'name';
    protected $_group       = 'group';

    function __construct()
    {
        $this->_db_hostname      = getenv('DB_HOSTNAME');
        $this->_db_database      = getenv('DB_DATABASE');
        $this->_db_username      = getenv('DB_USERNAME');
        $this->_db_password      = getenv('DB_PASSWORD');
        try {
            //Make sure you are using UTF-8
            $options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8');

            //Update the dbname username and password to suit your server
            $this->db = new PDO(
                'mysql:host='.$this->_db_hostname.';dbname='.$this->_db_database.'',
                $this->_db_username,
                $this->_db_password,
                $options
            );
            $this->db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,
                PDO::FETCH_ASSOC);

            //If you are using older version of PHP and having issues with Unicode
            //uncomment the following line
            //$this->db->exec("SET NAMES utf8");

        } catch (PDOException $e) {
            throw new RestException(501, 'MySQL: ' . $e->getMessage());
        }
    }

    function get_name()
    {
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sql = $this->db->query('SELECT '.$this->_name.' FROM '.$this->_table_name);
            return $sql->fetchAll();
        } catch (PDOException $e) {
            throw new RestException(501, 'MySQL: ' . $e->getMessage());
        }
    }

    function get($id)
    {
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sql = $this->db->prepare('SELECT * FROM '.$this->_table_name.' WHERE '.$this->_primary_key.' = :id');
            $sql->execute(array(':id' => $id));
            return $this->id2int($sql->fetch());
        } catch (PDOException $e) {
            throw new RestException(501, 'MySQL: ' . $e->getMessage());
        }
    }

    function get_by_name($name)
    {
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $sql = $this->db->prepare('SELECT * FROM '.$this->_table_name.' WHERE '.$this->_name.' = :name');
            $sql->execute(array(':name' => $name));
            return $this->id2int($sql->fetch());
        } catch (PDOException $e) {
            throw new RestException(501, 'MySQL: ' . $e->getMessage());
        }
    }

    private function id2int($r)
    {
        if (is_array($r)) {
            if (isset($r['id'])) {
                $r['id'] = intval($r['id']);
            } else {
                foreach ($r as &$r0) {
                    $r0['id'] = intval($r0['id']);
                }
            }
        }
        return $r;
    }
}

