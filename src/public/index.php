<?php

require_once './../vendor/restler.php';

use Luracast\Restler\Defaults;
use Luracast\Restler\Restler;

Defaults::$useUrlBasedVersioning = true;

$r = new Restler(true);
$dotenv = new Dotenv\Dotenv('../config');
$dotenv->load();
$r->setAPIVersion(1);
$r->addAPIClass('tracker');
$r->addAPIClass('client');
$r->addAPIClass('shopsystem');

$r->setOverridingFormats('HtmlFormat');
$r->handle();
